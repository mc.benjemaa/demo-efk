# Installing EFK stack on kubernetes

## Prerequisite

create PVs for elasticsearch cluster.


```shell
mkdir -p /mnt/data/efk-master && kubectl create -f pv-master.yaml
```



```shell
mkdir -p /mnt/data/efk-data && kubectl create -f pv-data.yaml
```


## first-step install elasticsearch

```shell
helm install stable/elasticsearch --name=elasticsearch --namespace=logs \
--set client.replicas=1 \
--set master.replicas=1 \
--set cluster.env.MINIMUM_MASTER_NODES=1 \
--set cluster.env.RECOVER_AFTER_MASTER_NODES=1 \
--set cluster.env.EXPECTED_MASTER_NODES=1 \
--set data.replicas=1 \
--set data.heapSize=300m \
--set master.persistence.storageClass=elasticsearch-master \
--set master.persistence.size=5Gi \
--set data.persistence.storageClass=elasticsearch-data \
--set data.persistence.size=5Gi
```



## install fluentbit 


```shell
helm install stable/fluent-bit --name=fluent-bit --namespace=logs --set backend.type=es --set backend.es.host=elasticsearch-client
```



## install kibana


```shell
helm install stable/kibana --name=kibana --namespace=logs --set env.ELASTICSEARCH_HOSTS=http://elasticsearch-client:9200 --set service.type=NodePort --set service.nodePort=31000
```


## watch until all components are up

be patient

## run a pod for testing the logs

```shell
kubectl run random-logger --image=chentex/random-logger --restart=Never
```

OR

```shell
kubectl run apache-logs --image=edsiper/apache_logs --restart=Never
```

## Access Kibana 


forward the port of kibana pod or use proxy
 
 OR expose it!
 
 
## Choose the index

choose index `kubernetes_cluster-*`

filter name: `@timestamp`


Kibana is ready!



### Others 

Annotation: 

 `annotations:
    fluentbit.io/parser: apache`

FluentBit annotation apache-logs

```shell
kubectl apply -f apache-logs.yaml
```

FluentBit exclude logs

```shell
kubectl apply -f apache-logs-exclude.yaml
```

 