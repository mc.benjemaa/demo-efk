# Installing EFK stack on kubernetes

## Prerequisite

create PVs for elasticsearch cluster.


```shell
mkdir -p /mnt/data/efk-master && kubectl create -f pv-master.yaml
```



```shell
mkdir -p /mnt/data/efk-data && kubectl create -f pv-data.yaml
```


create namespace `logging`

```shell
kubectl create ns logging
```


## Elasticsearch Operator


```shell
 helm repo add akomljen-charts \
    https://raw.githubusercontent.com/komljen/helm-charts/master/charts/
```

```shell
 helm repo update
```


```shell
helm install --name es-operator \
    --namespace logging \
    akomljen-charts/elasticsearch-operator
```


```shell
kubectl get pods -n logging
```


```shell
kubectl get CustomResourceDefinition
```


## installing the EFK stack using the helm chart


```shell
 helm install --name efk \
    --namespace logging \
    akomljen-charts/efk
```



The Elasticsearch curator job is installed as well as a cron job:

```shell
kubectl get cronjob -n logging
```




## watch until all components are up

be patient

## run a pod for testing the logs

```shell
kubectl run random-logger --image=chentex/random-logger --restart=Never
```

OR

```shell
kubectl run apache-logs --image=edsiper/apache_logs --restart=Never
```

## Access Kibana 


forward the port of kibana pod or use proxy
 
 
```shell
kubectl port-forward efk-kibana-6cf88598b6-xlkv2 5601 -n logging
``` 
 
 
## Choose the index

choose index `kubernetes_cluster-*`

filter name: `@timestamp`


Kibana is ready!



### Others 

Annotation: 

 `annotations:
    fluentbit.io/parser: apache`

FluentBit annotation apache-logs

```shell
kubectl apply -f apache-logs.yaml
```

FluentBit exclude logs

```shell
kubectl apply -f apache-logs-exclude.yaml
```

 